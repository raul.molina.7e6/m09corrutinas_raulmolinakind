import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

suspend fun main(){
    for(i in 0..20){
        mensajes(i)
    }
    println("Finished!")
}

suspend fun mensajes(num: Int){
    println(num)
    println("Hello World!")
    coroutineScope {
        launch {
            delay(100)
        }
    }
}