import kotlinx.coroutines.*

val listaDeTiempo = listOf<Int>(1000, 2000, 3000, 4000, 5000)

suspend fun main(){
    coroutineScope {
        launch {
            cavall1()
        }
        launch {
            cavall2()
        }
        launch {
            cavall3()
        }
    }
}
suspend fun cavall1(){
    for(i in 0..3){
        coroutineScope {
            launch {
                delay(listaDeTiempo.random().toLong())
                if(i == 0){
                    println("Caballo 1: Primer tramo")
                }else if(i == 1){
                    println("Caballo 1: Segundo tramo")
                }else if(i == 2){
                    println("Caballo 1: Tercer tramo")
                }else{
                    println("Caballo 1: Cuarto tramo")
                }
            }
        }
    }
    println("Caballo 1 ha acabado")
}
suspend fun cavall2(){
    for(i in 0..3){
        coroutineScope{
            launch {
                delay(listaDeTiempo.random().toLong())
                if(i == 0){
                    println("Caballo 2: Primer tramo")
                }else if(i == 1){
                    println("Caballo 2: Segundo tramo")
                }else if(i == 2){
                    println("Caballo 2: Tercer tramo")
                }else{
                    println("Caballo 2: Cuarto tramo")
                }
            }
        }
    }
    println("Caballo 2 ha acabado")
}
suspend fun cavall3(){
    for(i in 0..3){
        coroutineScope{
            launch {
                delay(listaDeTiempo.random().toLong())
                if(i == 0){
                    println("Caballo 3: Primer tramo")
                }else if(i == 1){
                    println("Caballo 3: Segundo tramo")
                }else if(i == 2){
                    println("Caballo 3: Tercer tramo")
                }else{
                    println("Caballo 3: Cuarto tramo")
                }
            }
        }
    }
    println("Caballo 3 ha acabado")
}