import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

suspend fun main(){
    for(i in 0..20){
        mensajes2(i)
        coroutineScope {
            launch {
                delay(100)
            }
        }
    }
    println("Finished!")
}

fun mensajes2(num: Int){
    println(num)
    println("Hello World!")
}