import kotlinx.coroutines.*

//1
/*
The main program is started
The main program continues
Background processing started
Background processing finished
The main program is finished
*/
//1.1
/*
The main program is started
The main program continues
Background processing started
The main program is finished
*/
//1.2
suspend fun doBackground(){
    withContext(Dispatchers.Default){
        launch {
            println("Background processing started")
            delay(1000)
            println("Background processing finished")
        }
    }
}
//1.3
fun main() {
    println("The main program is started")
    GlobalScope.launch {
        doBackground()
    }
    println("The main program continues")
    runBlocking {
        delay(1500)
        println("The main program is finished")
    }
}


