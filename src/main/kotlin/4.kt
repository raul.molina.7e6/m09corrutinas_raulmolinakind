import kotlinx.coroutines.*
import java.util.Scanner

suspend fun main(){
    val scanner = Scanner(System.`in`)
    val numeros = (1..50)
    val numAleatorio = numeros.random()
    var adivinado = false

    var juego = true

    GlobalScope.launch {
        delay(10000)
        juego = false
    }

    println("You have 10 seconds to guess the secret number...")

    while (juego){
        println("Escribe un numero:")
        val entrada = scanner.nextInt()
        if(entrada == numAleatorio){
            adivinado = true
            break
        }else if(entrada < numAleatorio){
            println("El numero secreto es mas grande")
        }else{
            println("El numero secreto es mas pequeño")
        }
    }
    println()
    if(adivinado) println("Felicidades has adivinado el numero :D")
    else println("Se acabó el tiempo, has perdido :(")
}