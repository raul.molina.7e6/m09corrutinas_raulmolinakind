import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.io.File
import kotlin.system.measureTimeMillis

suspend fun main(){
    val letra = File("C:\\Users\\rmoli\\Desktop\\prog\\ejesCorrutinasM09\\src\\main\\kotlin\\letraCancion").readLines()
    var tiempoTotal:Long = 0
    for(i in letra){
        val time = measureTimeMillis {
            coroutineScope {
                launch {
                    println(i)
                    delay(3000)
                }
            }
        }
        tiempoTotal+= time
    }

    println()
    GlobalScope.launch {
        val minutos = tiempoTotal/1000/60
        val minutosDeSegundos = (tiempoTotal/1000)%60
        println("$minutos:$minutosDeSegundos minutos")
    }
    println("${(tiempoTotal/1000)} segundos")

}